const inquirer = require('inquirer');

module.exports = {
  askForOrchestrator: (orchestratorList) => {
    const questions = [
      {
        type: 'rawlist',
        name: 'orchestrator',
        message: 'Select the orchestrator you want to test on:',
        choices: orchestratorList,
      },
    ];
    return inquirer.prompt(questions);
  },
};
