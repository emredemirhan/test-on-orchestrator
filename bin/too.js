#!/usr/bin/env node
'use strict';

const fs = require('fs-extra');
const inquirer = require('../lib/inquirer');
const copyDir = require('../lib/copydir');
const path = require('path');
const chalk = require('chalk');
const child_process = require('child_process');

try {
  var config = require(path.join(process.cwd(), '.tooConfig'));
} catch (e) {
  console.log(chalk.red.bold("Config file is not found. Please make sure that there is a config file in the root of your project called .tooConfig"));
  process.exit(1);
}

const startCopy = async () => {
    const selected = await inquirer.askForOrchestrator(config.orchestratorList);
    const destination_dist_path = "../"+selected.orchestrator+config.destination_dist_path
    if (fs.existsSync(destination_dist_path)) {
      // copy source folder to destination
      fs.copy(config.source_dist_path, destination_dist_path , function (err) {
        if (err) {
          console.log(chalk.red.bold('An error occured while copying the folder.'));
          return console.error(err);
        }
        console.log(chalk.green.bold('Copy completed!'));
      });
    } else {
        console.log(chalk.red.bold('Destination directory not found. Please make sure that the orchestrator you selected is already installed.'));
        startCopy();
    }
  };
  
// START
console.log(chalk.blue.bold('Build process is starting...'));
const build_process = child_process.exec(config.build_command, function (error, stdout, stderr) {
if (error) throw error;
});

build_process.on('exit', function (code, signal) {
  console.clear();
  startCopy();
}); 
