# Test On Orchestrator

This project is a simple CLI tool to get a temporary build from a shared component (ex: payment-component) and copy it into the orchestrator locally to test the flow. 

### Set up

In order to use this cli tool you need to add the following lines into the devDependencies and scripts part of your package.json file where your shared component is.

````javascript
"scripts":{
...
"test-on-orchestrator" : "too"
...
},
"devDependencies": {
...
"@musement/test-on-orchestrator": "git@gitlab.com:emredemirhan/test-on-orchestrator.git",
...
}
````
and after that just run `$ npm install` to install the dependencies

### Sample Config file　
In order for cli to work you need to add a file called ".tooConfig" into the root directory of your shared component. Here is an example file :
```javascript
var config = {};

config.destination_dist_path = "/node_modules/@musement/payment-component/dist";
config.source_dist_path = "./dist";
config.build_command = "npm run build";
config.orchestratorList = [
    { value: 'b2c-tui', name: 'GoTui' },
    { value: 'b2c-frontend', name: 'Musement' },
    { value: 'txc-frontend', name : 'TXC'},
    { value: 'cruise-b2c-frontend', name : 'River Cruise'},
    { value: 'white-label-frontend', name : 'White Labels'},
    { value: 'non-existent', name : 'Non Existing Project'},
  ];
module.exports = config;
```

                    
Property  | Description
------------- | -------------
destination_dist_path  | This is the destination path where your shared component dist folder is located in the orchestrator 
source_dist_path  | This is the path for your shared component's dist folder
build_command | This is the command that you use to build the shared component in the package.json file of the component
orchestratorList | This is the array of objects for the destination path of various orchestrators in your local machine. Remember : your shared component and orchestrators must be in the same root folder. 
                
### How to run?
After everything is properly configured, you need to run `$ npm run test-on-orchestrator` to run the CLI.

### What's next?
We need to figure out how can we create the test feature branch for the orchestrator with skipping the build command in the CI/CD